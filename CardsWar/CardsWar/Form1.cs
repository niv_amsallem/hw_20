﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardsWar
{
    public partial class Form1 : Form
    {
        Random random = new Random();
        List<Image> icons = new List<Image>()
        {
            CardsWar.Properties.Resources.ace_of_clubs, CardsWar.Properties.Resources.ace_of_diamonds , CardsWar.Properties.Resources.ace_of_hearts , CardsWar.Properties.Resources.ace_of_spades2
            ,CardsWar.Properties.Resources._2_of_clubs ,CardsWar.Properties.Resources._2_of_diamonds ,CardsWar.Properties.Resources._2_of_hearts ,CardsWar.Properties.Resources._2_of_spades
            ,CardsWar.Properties.Resources._3_of_clubs ,CardsWar.Properties.Resources._3_of_diamonds ,CardsWar.Properties.Resources._3_of_hearts ,CardsWar.Properties.Resources._3_of_spades
            ,CardsWar.Properties.Resources._4_of_clubs ,CardsWar.Properties.Resources._4_of_diamonds ,CardsWar.Properties.Resources._4_of_hearts ,CardsWar.Properties.Resources._4_of_spades
            ,CardsWar.Properties.Resources._5_of_clubs,CardsWar.Properties.Resources._5_of_diamonds ,CardsWar.Properties.Resources._5_of_hearts ,CardsWar.Properties.Resources._5_of_spades
            ,CardsWar.Properties.Resources._6_of_clubs ,CardsWar.Properties.Resources._6_of_diamonds ,CardsWar.Properties.Resources._6_of_hearts ,CardsWar.Properties.Resources._6_of_spades
            ,CardsWar.Properties.Resources._7_of_clubs ,CardsWar.Properties.Resources._7_of_diamonds ,CardsWar.Properties.Resources._7_of_hearts,CardsWar.Properties.Resources._7_of_spades 
            ,CardsWar.Properties.Resources._8_of_clubs,CardsWar.Properties.Resources._8_of_diamonds ,CardsWar.Properties.Resources._8_of_hearts ,CardsWar.Properties.Resources._8_of_spades 
            ,CardsWar.Properties.Resources._9_of_clubs ,CardsWar.Properties.Resources._9_of_diamonds ,CardsWar.Properties.Resources._9_of_hearts ,CardsWar.Properties.Resources._9_of_spades
            ,CardsWar.Properties.Resources._10_of_clubs ,CardsWar.Properties.Resources._10_of_diamonds ,CardsWar.Properties.Resources._10_of_hearts ,CardsWar.Properties.Resources._10_of_spades
            ,CardsWar.Properties.Resources.jack_of_clubs2, CardsWar.Properties.Resources.jack_of_diamonds2 , CardsWar.Properties.Resources.jack_of_hearts2 ,CardsWar.Properties.Resources.jack_of_spades2
            ,CardsWar.Properties.Resources.queen_of_clubs2 ,CardsWar.Properties.Resources.queen_of_diamonds2 ,CardsWar.Properties.Resources.queen_of_hearts2 ,CardsWar.Properties.Resources.queen_of_spades2
            ,CardsWar.Properties.Resources.king_of_clubs2, CardsWar.Properties.Resources.king_of_diamonds2 , CardsWar.Properties.Resources.king_of_hearts2 ,CardsWar.Properties.Resources.king_of_spades2 
              
        };
        int rand;
        List<Image> cards = new List<Image>();
        public Form1()
        {
            InitializeComponent();
        }

        public Form1(IContainer components, Label yscore, Label oscore, PictureBox pictureBox1, Button forfeit, PictureBox p1, PictureBox p4, PictureBox p5, PictureBox p6, PictureBox p7, PictureBox p8, PictureBox p9, PictureBox p10, PictureBox p2, PictureBox p3, Random random, List<string> icons)
        {
            this.components = components;
            Yscore = yscore;
            Oscore = oscore;
            this.pictureBox1 = pictureBox1;
            Forfeit = forfeit;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.random = random;
        }

        public void GenerateCards()
        {
           
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void Forfeit_Click(object sender, EventArgs e)
        {
            if (false)
            {
                MessageBox.Show(" You Win!!!", "The Winner...", MessageBoxButtons.OKCancel);
            }
            else
            {
                MessageBox.Show(" Is'nt you!!!\n f*cking Loser XD", "The Winner...", MessageBoxButtons.OKCancel);
            }
            this.Close();
        }

        private void P1_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p1.Image = icons[rand];
        }

        private void P2_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p2.Image = icons[rand];
        }

        private void P3_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p3.Image = icons[rand];
        }

        private void P4_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p4.Image = icons[rand];
        }

        private void P5_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p5.Image = icons[rand];
        }

        private void P6_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p6.Image = icons[rand];
        }

        private void P7_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p7.Image = icons[rand];
        }

        private void P8_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p8.Image = icons[rand];
        }

        private void P9_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p9.Image = icons[rand];
        }

        private void P10_Click(object sender, EventArgs e)
        {
            rand = random.Next(0, icons.Count);
            p10.Image = icons[rand];
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(true)
            {
                MessageBox.Show(" You Win!!!", "The Winner...", MessageBoxButtons.OKCancel);
            }
            else
            {
                MessageBox.Show(" Is'nt you!!!\n f*cking Loser XD", "The Winner...", MessageBoxButtons.OKCancel);
            }
        }
    }
}
