﻿namespace CardsWar
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Yscore = new System.Windows.Forms.Label();
            this.Oscore = new System.Windows.Forms.Label();
            this.Forfeit = new System.Windows.Forms.Button();
            this.p3 = new System.Windows.Forms.PictureBox();
            this.p2 = new System.Windows.Forms.PictureBox();
            this.p10 = new System.Windows.Forms.PictureBox();
            this.p9 = new System.Windows.Forms.PictureBox();
            this.p8 = new System.Windows.Forms.PictureBox();
            this.p7 = new System.Windows.Forms.PictureBox();
            this.p6 = new System.Windows.Forms.PictureBox();
            this.p5 = new System.Windows.Forms.PictureBox();
            this.p4 = new System.Windows.Forms.PictureBox();
            this.p1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.p3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Yscore
            // 
            this.Yscore.AutoSize = true;
            this.Yscore.Location = new System.Drawing.Point(14, 22);
            this.Yscore.Name = "Yscore";
            this.Yscore.Size = new System.Drawing.Size(72, 13);
            this.Yscore.TabIndex = 0;
            this.Yscore.Text = "Your Score: 0";
            // 
            // Oscore
            // 
            this.Oscore.AutoSize = true;
            this.Oscore.Location = new System.Drawing.Point(497, 17);
            this.Oscore.Name = "Oscore";
            this.Oscore.Size = new System.Drawing.Size(104, 13);
            this.Oscore.TabIndex = 1;
            this.Oscore.Text = "Opponent\'s Score: 0";
            // 
            // Forfeit
            // 
            this.Forfeit.Location = new System.Drawing.Point(265, 12);
            this.Forfeit.Name = "Forfeit";
            this.Forfeit.Size = new System.Drawing.Size(75, 23);
            this.Forfeit.TabIndex = 3;
            this.Forfeit.Text = "Forfeit";
            this.Forfeit.UseVisualStyleBackColor = true;
            this.Forfeit.Click += new System.EventHandler(this.Forfeit_Click);
            // 
            // p3
            // 
            this.p3.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p3.Location = new System.Drawing.Point(135, 173);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(53, 88);
            this.p3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p3.TabIndex = 13;
            this.p3.TabStop = false;
            this.p3.Click += new System.EventHandler(this.P3_Click);
            // 
            // p2
            // 
            this.p2.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p2.Location = new System.Drawing.Point(76, 173);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(53, 88);
            this.p2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p2.TabIndex = 12;
            this.p2.TabStop = false;
            this.p2.Click += new System.EventHandler(this.P2_Click);
            // 
            // p10
            // 
            this.p10.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p10.Location = new System.Drawing.Point(548, 173);
            this.p10.Name = "p10";
            this.p10.Size = new System.Drawing.Size(53, 88);
            this.p10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p10.TabIndex = 11;
            this.p10.TabStop = false;
            this.p10.Click += new System.EventHandler(this.P10_Click);
            // 
            // p9
            // 
            this.p9.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p9.Location = new System.Drawing.Point(489, 173);
            this.p9.Name = "p9";
            this.p9.Size = new System.Drawing.Size(53, 88);
            this.p9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p9.TabIndex = 10;
            this.p9.TabStop = false;
            this.p9.Click += new System.EventHandler(this.P9_Click);
            // 
            // p8
            // 
            this.p8.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p8.Location = new System.Drawing.Point(430, 173);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(53, 88);
            this.p8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p8.TabIndex = 9;
            this.p8.TabStop = false;
            this.p8.Click += new System.EventHandler(this.P8_Click);
            // 
            // p7
            // 
            this.p7.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p7.Location = new System.Drawing.Point(371, 173);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(53, 88);
            this.p7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p7.TabIndex = 8;
            this.p7.TabStop = false;
            this.p7.Click += new System.EventHandler(this.P7_Click);
            // 
            // p6
            // 
            this.p6.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p6.Location = new System.Drawing.Point(312, 173);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(53, 88);
            this.p6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p6.TabIndex = 7;
            this.p6.TabStop = false;
            this.p6.Click += new System.EventHandler(this.P6_Click);
            // 
            // p5
            // 
            this.p5.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p5.Location = new System.Drawing.Point(253, 173);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(53, 88);
            this.p5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p5.TabIndex = 6;
            this.p5.TabStop = false;
            this.p5.Click += new System.EventHandler(this.P5_Click);
            // 
            // p4
            // 
            this.p4.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p4.Location = new System.Drawing.Point(194, 173);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(53, 88);
            this.p4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p4.TabIndex = 5;
            this.p4.TabStop = false;
            this.p4.Click += new System.EventHandler(this.P4_Click);
            // 
            // p1
            // 
            this.p1.Image = global::CardsWar.Properties.Resources.card_back_red;
            this.p1.Location = new System.Drawing.Point(17, 173);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(53, 88);
            this.p1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p1.TabIndex = 4;
            this.p1.TabStop = false;
            this.p1.Click += new System.EventHandler(this.P1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CardsWar.Properties.Resources.card_back_blue;
            this.pictureBox1.Location = new System.Drawing.Point(275, 41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(55, 77);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(614, 291);
            this.Controls.Add(this.p3);
            this.Controls.Add(this.p2);
            this.Controls.Add(this.p10);
            this.Controls.Add(this.p9);
            this.Controls.Add(this.p8);
            this.Controls.Add(this.p7);
            this.Controls.Add(this.p6);
            this.Controls.Add(this.p5);
            this.Controls.Add(this.p4);
            this.Controls.Add(this.p1);
            this.Controls.Add(this.Forfeit);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Oscore);
            this.Controls.Add(this.Yscore);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.p3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Yscore;
        private System.Windows.Forms.Label Oscore;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Forfeit;
        private System.Windows.Forms.PictureBox p1;
        private System.Windows.Forms.PictureBox p4;
        private System.Windows.Forms.PictureBox p5;
        private System.Windows.Forms.PictureBox p6;
        private System.Windows.Forms.PictureBox p7;
        private System.Windows.Forms.PictureBox p8;
        private System.Windows.Forms.PictureBox p9;
        private System.Windows.Forms.PictureBox p10;
        private System.Windows.Forms.PictureBox p2;
        private System.Windows.Forms.PictureBox p3;
    }
}

